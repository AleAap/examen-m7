import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

public class JUnitTest { 

	@Test
	public void test() {
		Primordial pri = new Primordial();
		Assert.assertTrue(pri.get_primordial(8.0)>200);
		Assert.assertFalse(pri.get_primordial(6.0)>30);
		Assert.assertTrue(pri.get_primordial(2.0)<=2);
	}

}
